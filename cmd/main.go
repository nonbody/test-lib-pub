package main

import (
	"gitlab.com/go-market/http-demo-lib/internal/ben"
	"gitlab.com/go-market/http-demo-lib/internal/jim"
	"gitlab.com/go-market/http-demo-lib/pkg/peter"
)

func main() {
	jim.IntroJim()
	ben.IntroBen()
	peter.IntroPeter()
}
